import 'package:state_management_redux/partial_refresh/partial_refresh_action.dart';

import 'partitial_refresh_state.dart';

PartialRefreshState partialRefreshReducer(
    PartialRefreshState state, dynamic action) {
  if (action is FavorAction) {
    return PartialRefreshState(
        favorCount: state.favorCount + 1, praiseCount: state.praiseCount);
  }

  if (action is PraiseAction) {
    return PartialRefreshState(
        favorCount: state.favorCount, praiseCount: state.praiseCount + 1);
  }

  return state;
}
