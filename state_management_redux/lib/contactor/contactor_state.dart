class ContactorState {
  final List<dynamic> contactors;
  final isLoading;
  final String? errorMessage;
  final int currentPage;

  ContactorState(this.contactors,
      {this.isLoading = false, this.errorMessage, this.currentPage = 1});

  factory ContactorState.initial() => ContactorState(List.unmodifiable([]));
}
