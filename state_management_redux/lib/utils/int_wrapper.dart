class IntWrapper {
  final int value;
  IntWrapper(this.value);

  @override
  operator ==(Object? other) {
    if (other == null || !(other is IntWrapper)) return false;
    if (other.value != value) return false;

    return true;
  }

  @override
  get hashCode {
    print(value.hashCode);
    return value.hashCode;
  }

  @override
  toString() => value.toString();
}
