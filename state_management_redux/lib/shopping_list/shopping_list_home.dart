import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:state_management_redux/components/cart_number.dart';
import 'package:state_management_redux/partial_refresh/dynamic_detail.dart';
import 'shopping_list_action.dart';
import 'shopping_list_add.dart';
import 'shopping_list_state.dart';

class ShoppingListHome extends StatelessWidget {
  const ShoppingListHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint('build: ShoppingListHome');
    return Scaffold(
      appBar: AppBar(
        title: Text('购物清单'),
        actions: [
          IconButton(
            onPressed: () {
              print('Switch');
              StoreProvider.of<ShoppingListState>(context)
                  .dispatch(NoStateChangeAction());
            },
            icon: Icon(Icons.switch_left_outlined),
          ),
          IconButton(
            onPressed: () {
              print('Navigation');
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => DynamicDetailWrapper()));
            },
            icon: Icon(Icons.arrow_right),
          ),
        ],
      ),
      body: StoreConnector<ShoppingListState, List<Widget>>(
        converter: (store) => _ShoppListViewModel.build(store),
        builder: (context, items) => ListView.builder(
          itemBuilder: (context, index) => items[index],
          itemCount: items.length,
        ),
      ),
      bottomSheet: _BottomStatisticBar(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => _openAddItemDialog(context),
      ),
    );
  }
}

class _ShoppListViewModel {
  static build(Store<ShoppingListState> store) {
    return store.state.shoppingItems
        .map(
          (item) => Container(
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Checkbox(
                  value: item.selected,
                  onChanged: (value) {
                    store.dispatch(ToggleItemStateAction(item: item));
                  },
                ),
                Expanded(
                  child: Text(item.name),
                ),
                CartNumber(
                  count: item.count,
                  onAdd: (count) {
                    store.dispatch(AddItemCountAction(item: item));
                  },
                  onSub: (count) {
                    store.dispatch(SubItemCountAction(item: item));
                  },
                ),
              ],
            ),
          ),
        )
        .toList();
  }
}

class _BottomStatisticBar extends StatelessWidget {
  const _BottomStatisticBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint('build: _BottomStatisticBar');
    return StoreConnector<ShoppingListState, int>(
      builder: (context, selectedCount) => Container(
        height: 60,
        width: double.infinity,
        color: Colors.grey,
        padding: EdgeInsets.all(10),
        child: Text('已完成$selectedCount项'),
      ),
      converter: (store) =>
          store.state.shoppingItems.where((item) => item.selected).length,
    );
  }
}

void _openAddItemDialog(BuildContext context) {
  showDialog(context: context, builder: (context) => AddItemDialog());
}
