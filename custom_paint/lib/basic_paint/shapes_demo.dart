import 'dart:math';

import 'package:flutter/material.dart';

class ShapesDemo extends StatelessWidget {
  const ShapesDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: ShapesPainter(),
    );
  }
}

class ShapesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawColor(Color(0xFFF1F1F1), BlendMode.color);
    var center = size / 2;
    // 绘制矩形
    var paint = Paint()..color = Color(0xFF2080E5);
    paint.strokeWidth = 2.0;
    canvas.drawRect(
      Rect.fromLTWH(center.width - 100, 60, 200, 120),
      paint,
    );

    // 绘制圆形
    paint.color = Color(0xFFE58020);
    paint.strokeWidth = 1.0;
    canvas.drawCircle(
      Offset(center.width - 80, 240),
      40,
      paint,
    );

    // 绘制椭圆
    canvas.drawOval(
      Rect.fromLTWH(center.width, 200, 120, 80),
      paint,
    );

    // 使用 Path绘制三角形
    Path trianglePath = Path();
    // 空心绘制
    paint.style = PaintingStyle.stroke;
    trianglePath.moveTo(center.width - 30, 300);
    trianglePath.lineTo(center.width + 30, 300);
    trianglePath.lineTo(center.width, 352);
    trianglePath.lineTo(center.width - 30, 300);
    canvas.drawPath(trianglePath, paint);

    // 绘制弧线
    paint.style = PaintingStyle.fill;
    paint.color = Color(0xFFE53000);
    canvas.drawArc(
      Rect.fromLTWH(center.width - 60, 340, 120, 80),
      0,
      pi / 2,
      false,
      paint,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
