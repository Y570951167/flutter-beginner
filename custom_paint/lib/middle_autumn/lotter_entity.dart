class LotteryEntity {
  final String assetName;
  final String name;
  final int probability;

  LotteryEntity({
    required this.name,
    required this.assetName,
    required this.probability,
  });
}
