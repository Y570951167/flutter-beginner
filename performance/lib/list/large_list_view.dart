import 'package:flutter/material.dart';

class LargeListView extends StatefulWidget {
  const LargeListView({Key? key}) : super(key: key);

  @override
  _LargeListViewState createState() => _LargeListViewState();
}

class _LargeListViewState extends State<LargeListView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('大列表'),
        brightness: Brightness.dark,
      ),
      body: ListView.builder(
        itemBuilder: (context, index) => ListItem(
          index: index,
        ),
        itemCount: 1000,
        addAutomaticKeepAlives: false,
        addRepaintBoundaries: false,
        itemExtent: 120.0,
      ),
    );
  }
}

class ListItem extends StatelessWidget {
  final int index;
  ListItem({Key? key, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      child: Row(
        children: [
          const ListImage(),
          const SizedBox(
            width: 5.0,
          ),
          Text('第$index 个元素'),
        ],
      ),
      padding: EdgeInsets.all(10.0),
    );
  }
}

class ListImage extends StatelessWidget {
  const ListImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      'https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/7869eac08a7d4177b600dc7d64998204~tplv-k3u1fbpfcp-watermark.jpeg',
      width: 200,
    );
  }
}
