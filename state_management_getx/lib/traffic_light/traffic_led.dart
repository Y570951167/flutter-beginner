import 'dart:async';

import 'package:flutter/material.dart';

import 'package:get/get.dart';

enum TrafficLight { green, red, yellow }

class TrafficLightController extends GetxController {
  late TrafficLight _currentLight;
  get currentLight => _currentLight;

  int _counter = 0;
  get counter => _counter;

  late Timer _downcountTimer;

  @override
  void onInit() {
    _counter = 20;
    _currentLight = TrafficLight.green;
    super.onInit();
  }

  @override
  void onReady() {
    _downcountTimer = Timer.periodic(Duration(seconds: 1), decreament);
    super.onReady();
  }

  void decreament(Timer timer) {
    _counter--;
    if (_counter == 0) {
      switch (_currentLight) {
        case TrafficLight.green:
          _currentLight = TrafficLight.yellow;
          _counter = 3;
          update(['green', 'yellow']);
          break;
        case TrafficLight.yellow:
          _currentLight = TrafficLight.red;
          _counter = 10;
          update(['red', 'yellow']);
          break;
        case TrafficLight.red:
          _currentLight = TrafficLight.green;
          _counter = 20;
          update(['red', 'green']);
          break;
      }
    } else {
      switch (_currentLight) {
        case TrafficLight.green:
          update(['green']);
          break;
        case TrafficLight.yellow:
          update(['yellow']);
          break;
        case TrafficLight.red:
          update(['red']);
          break;
      }
    }
  }

  @override
  void onClose() {
    _downcountTimer.cancel();
    super.onClose();
  }
}

class TrafficLedPage extends StatelessWidget {
  TrafficLedPage({Key? key}) : super(key: key);
  final TrafficLightController lightController = TrafficLightController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('红绿灯'),
      ),
      body: Center(
        child: Container(
          width: 240,
          height: 100,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Color(0xFF303030),
            borderRadius: BorderRadius.circular(12.0),
            boxShadow: [
              BoxShadow(
                color: Color(0xFF808080),
                offset: Offset(2, 4),
                blurRadius: 6.0,
              )
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GetBuilder<TrafficLightController>(
                id: 'green',
                init: lightController,
                builder: (state) => TrafficLed(
                  ledColor: (state.currentLight == TrafficLight.green
                      ? Colors.green
                      : Colors.black),
                  secondsLeft: state.counter,
                  showSeconds: state.currentLight == TrafficLight.green,
                ),
              ),
              GetBuilder<TrafficLightController>(
                id: 'yellow',
                init: lightController,
                builder: (state) => TrafficLed(
                  ledColor: (state.currentLight == TrafficLight.yellow
                      ? Colors.yellow[600]!
                      : Colors.black),
                  secondsLeft: state.counter,
                  showSeconds: state.currentLight == TrafficLight.yellow,
                ),
              ),
              GetBuilder<TrafficLightController>(
                id: 'red',
                init: lightController,
                builder: (state) => TrafficLed(
                  ledColor: (state.currentLight == TrafficLight.red
                      ? Colors.red[600]!
                      : Colors.black),
                  secondsLeft: state.counter,
                  showSeconds: state.currentLight == TrafficLight.red,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TrafficLed extends StatelessWidget {
  final Color ledColor;
  final int secondsLeft;
  final bool showSeconds;
  final double ledSize;
  const TrafficLed({
    Key? key,
    required this.ledColor,
    required this.secondsLeft,
    required this.showSeconds,
    this.ledSize = 60.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        alignment: Alignment.center,
        width: ledSize,
        height: ledSize,
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(ledSize / 2),
          boxShadow: [
            BoxShadow(
              color: Color(0xFF505050),
              offset: Offset(1, -1),
              blurRadius: 0.2,
            )
          ],
        ),
        child: Offstage(
          child: Text(
            '$secondsLeft',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: this.ledColor,
              fontSize: 36,
              fontWeight: FontWeight.bold,
            ),
          ),
          offstage: !showSeconds,
        ),
      ),
    );
  }
}
