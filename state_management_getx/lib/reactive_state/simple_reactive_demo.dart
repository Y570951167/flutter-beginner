import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SimpleReactiveController extends GetxController {
  final _name = ' 岛上码农'.obs;
  set name(value) => this._name.value = value;
  get name => this._name.value;
}

class SimpleReactivePage extends StatelessWidget {
  SimpleReactivePage({Key? key}) : super(key: key);
  final SimpleReactiveController simpleController = SimpleReactiveController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('响应式状态管理'),
      ),
      body: Center(
        child: GetX<SimpleReactiveController>(
          builder: (controller) => Text('${simpleController.name}'),
          init: simpleController,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        onPressed: () {
          simpleController.name = 'island-coder';
        },
      ),
    );
  }
}
