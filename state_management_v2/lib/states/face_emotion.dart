class FaceEmotion {
  final String emotion;
  const FaceEmotion({this.emotion = '平静'});

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    if (other.runtimeType != runtimeType) {
      return false;
    }
    // ignore: test_types_in_equals
    final FaceEmotion otherModel = other as FaceEmotion;
    return otherModel.emotion == emotion;
  }

  @override
  int get hashCode => emotion.hashCode;
}
