class ContactorEntity {
  late String _id;
  late String _nickname;
  late String _avatar;
  late String _description;

  String get id => _id;
  String get nickname => _nickname;
  String get avatar => _avatar;
  String get description => _description;

  static ContactorEntity fromJson(Map<String, dynamic> json) {
    ContactorEntity contacotr = ContactorEntity();
    contacotr._id = json['_id'];
    contacotr._nickname = json['nickname'];
    contacotr._avatar = json['avatar'];
    contacotr._description = json['description'];
    return contacotr;
  }
}
