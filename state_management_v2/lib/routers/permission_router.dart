import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';

class PermissionRouter extends FluroRouter {
  PermissionRouter._privateContructor();
  static final PermissionRouter pernissionRouter =
      PermissionRouter._privateContructor();
  late List<String>? _whiteList; //白名单路由路径
  set whiteList(value) => _whiteList = value;

  late String? _permissionDeniedPath; //拦截后跳转的路由路径
  set permissionDeniedPath(value) => _permissionDeniedPath = value;

  @override
  Future navigateTo(
    BuildContext context,
    String path, {
    bool replace = false,
    bool clearStack = false,
    bool maintainState = true,
    bool rootNavigator = false,
    TransitionType? transition,
    Duration? transitionDuration,
    transitionBuilder,
    RouteSettings? routeSettings,
  }) {
    String pathToNavigate = path;
    // 屏蔽路由拦截
    // AppRouteMatch routeMatched = this.match(path);
    // String routePathMatched = routeMatched?.route?.route;
    // if (routePathMatched != null) {
    //   //设置了白名单且当前路由不在白名单内
    //   if (_whiteList != null && !_whiteList.contains(routePathMatched)) {
    //     pathToNavigate = _permissionDeniedPath;
    //   }
    // }
    return super.navigateTo(context, pathToNavigate,
        replace: replace,
        clearStack: clearStack,
        maintainState: maintainState,
        rootNavigator: rootNavigator,
        transition: transition,
        transitionDuration: transitionDuration,
        transitionBuilder: transitionBuilder,
        routeSettings: routeSettings);
  }
}
