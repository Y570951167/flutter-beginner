import 'package:flutter/material.dart';

class AnimatedPhysicalModelDemo extends StatefulWidget {
  const AnimatedPhysicalModelDemo({Key? key}) : super(key: key);

  @override
  _AnimatedPhysicalModelDemoState createState() =>
      _AnimatedPhysicalModelDemoState();
}

class _AnimatedPhysicalModelDemoState extends State<AnimatedPhysicalModelDemo> {
  var _elevation = 0.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AnimatedPhysicalModel 动画'),
      ),
      body: Center(
        child: AnimatedPhysicalModel(
          child: Container(
            width: 300,
            height: 300,
          ),
          duration: Duration(seconds: 1),
          color: _elevation == 0.0 ? Colors.blue : Colors.green,
          animateColor: true,
          animateShadowColor: true,
          elevation: _elevation,
          shape: BoxShape.circle,
          shadowColor: Colors.blue[900]!,
          curve: Curves.easeInOutCubic,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Text(
          'Play',
          style: TextStyle(
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
        onPressed: () {
          setState(() {
            _elevation = _elevation == 0 ? 10.0 : 0.0;
          });
        },
      ),
    );
  }
}
