import 'package:animation/advance_animation/rolling_wheel.dart';
import 'package:flutter/material.dart';

import '../hero/list_to_detail_hero.dart';
import '../hero/flight_shuttle_builder_hero.dart';
import '../hero/radial_hero_animation.dart';
import '../hero/custom_rect_tween_hero.dart';
import '../hero/inkwell_hero_aniamtion.dart';
import '../hero/hero_index.dart';
import '../transition_animation/cupertino_page_transition_demo.dart';
import '../transition_animation/cupertino_fullscreen_dialog_transition_demo.dart';
import '../transition_animation/scale_transition_demo.dart';
import '../advance_animation/animated_list_demo.dart';
import '../advance_animation/staggered_animation_demo.dart';
import '../basic_animation/tween_animation_demo.dart';
import '../transition_animation/size_transition_demo.dart';
import '../transition_animation/slide_transition_demo.dart';
import '../chinese_word/chinese_word_animation.dart';
import '../windmill/windmill_indicator_demo.dart';
import '../components/primary_button.dart';

import 'animated_builder_demo.dart';
import 'animated_container_demo.dart';
import 'animated_modal_barrier_demo.dart';
import 'animated_opacity_demo.dart';
import 'animated_padding_demo.dart';
import 'animated_physical_model_demo.dart';
import 'animated_positioned_demo.dart';
import 'animated_switcher_demo.dart';
import 'animated_widget_demo.dart';
import 'animation_demo.dart';
import 'custom_curve_demo.dart';
import 'rocket_launch.dart';

class AnimationHomePage extends StatelessWidget {
  const AnimationHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('基本动画'),
      ),
      body: ListView(
        children: [
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => const AnimtionDemo(),
                ),
              );
            },
            title: 'Animation方式',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => const AnimatedWidgetDemo(),
                ),
              );
            },
            title: 'AnimatedWidget方式',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      const AnimatedBuilderDemo1(),
                ),
              );
            },
            title: 'Animated Builder方式',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      const WindmillIndicatorDemo(),
                ),
              );
            },
            title: '大风车动画',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      const AnimatedOpacityDemo(),
                ),
              );
            },
            title: '透明度动画',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => const SwtichImageDemo(),
                ),
              );
            },
            title: '图片切换',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      const ChineseWordAnimation(),
                ),
              );
            },
            title: '汉字点阵',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => AnimatedContainerDemo(),
                ),
              );
            },
            title: '笑嘻嘻动画',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => CustomCurveDemo(),
                ),
              );
            },
            title: '自定义曲线',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => RocketLaunch(),
                ),
              );
            },
            title: '火箭发射',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => AnimatedPaddingDemo(),
                ),
              );
            },
            title: 'AnimatedPadding',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => AnimatedSwitcherDemo(),
                ),
              );
            },
            title: 'AnimatedSwticher',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => RepeatBall(),
                ),
              );
            },
            title: 'TweenAnimationBuilder',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => AnimatedPositionedDemo(),
                ),
              );
            },
            title: '追逐球',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => AnimatedListSample(),
                ),
              );
            },
            title: 'AnimatedList',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => SlideTransitionDemo(),
                ),
              );
            },
            title: 'SlideTransition',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => SizeTransitionDemo(),
                ),
              );
            },
            title: 'SizeTransition',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => ScaleTransitionDemo(),
                ),
              );
            },
            title: 'ScaleTransition',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => AnimatedModelBarrierDemo(),
                ),
              );
            },
            title: 'AnimatedModelBarrierDemo',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      AnimatedPhysicalModelDemo(),
                ),
              );
            },
            title: 'AnimatedPhysicalModelDemo',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      CupertinoFullscreenDialogTransitionDemo(),
                ),
              );
            },
            title: 'CupertinoFullscreenDialogTransitionDemo',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      CupertinoPageTransitionDemo(),
                ),
              );
            },
            title: 'CupertinoPageTransitionDemo',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => HeroIndexPage(),
                ),
              );
            },
            title: 'Hero 基础动画',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => CustomRectTweenHero(),
                ),
              );
            },
            title: 'Hero 路径',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => FlightShuttleBuilderHero(),
                ),
              );
            },
            title: 'FlightShuttleBuilder',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => HeroListDemo(),
                ),
              );
            },
            title: 'HeroListDemo',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => StaggeredAnimationDemo(),
                ),
              );
            },
            title: '交错动画',
          ),
          PrimaryButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => RollingWheel(),
                ),
              );
            },
            title: '交错动画示例',
          ),
        ],
      ),
    );
  }
}
