import 'dart:math';

import 'package:animation/custom_curves/sine_curve.dart';
import 'package:flutter/material.dart';

class CustomCurveDemo extends StatefulWidget {
  CustomCurveDemo({Key? key}) : super(key: key);

  @override
  _CustomCurveDemoState createState() => _CustomCurveDemoState();
}

class _CustomCurveDemoState extends State<CustomCurveDemo> {
  var up = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('自定义曲线'),
        brightness: Brightness.dark,
      ),
      body: Center(
        child: AnimatedContainer(
          decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.circular(30.0),
          ),
          transform: Matrix4.identity()..translate(0.0, up ? 60.0 : 0.0, 0.0),
          duration: Duration(milliseconds: 5000),
          curve: SineCurve(count: 1),
          child: ClipOval(
            child: Container(
              width: 60.0,
              height: 60.0,
              color: Colors.blue,
            ),
          ),
          // onEnd: () {
          //   setState(() {
          //     up = !up;
          //   });
          // },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_arrow, color: Colors.white),
        onPressed: () {
          setState(() {
            up = !up;
          });
        },
      ),
    );
  }
}
