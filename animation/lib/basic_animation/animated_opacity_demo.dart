import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

class AnimatedOpacityDemo extends StatefulWidget {
  const AnimatedOpacityDemo({Key? key}) : super(key: key);

  @override
  _AnimatedOpacityDemoState createState() => _AnimatedOpacityDemoState();
}

class _AnimatedOpacityDemoState extends State<AnimatedOpacityDemo> {
  var opacity = 0.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AnimatedOpacity 动画'),
      ),
      body: Center(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Text('小姐姐在哪'),
            AnimatedOpacity(
              duration: Duration(seconds: 3),
              opacity: opacity,
              child: ClipOval(
                child: Image.asset(
                  'images/beauty.jpeg',
                  width: 300,
                  height: 300,
                ),
              ),
              curve: Curves.ease,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Text(
          opacity == 0 ? 'Show' : 'Hide',
          style: TextStyle(
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
        onPressed: () {
          setState(() {
            opacity = opacity == 0 ? 1.0 : 0.0;
          });
        },
      ),
    );
  }
}

class SwtichImageDemo extends StatefulWidget {
  const SwtichImageDemo({Key? key}) : super(key: key);

  @override
  _SwtichImageDemoState createState() => _SwtichImageDemoState();
}

class _SwtichImageDemoState extends State<SwtichImageDemo> {
  var opacity1 = 1.0;
  var opacity2 = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('图片切换'),
        brightness: Brightness.dark,
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Stack(
          alignment: Alignment.center,
          children: [
            AnimatedOpacity(
              duration: Duration(milliseconds: 5000),
              opacity: opacity1,
              child: ClipOval(
                child: Image.asset(
                  'images/beauty.jpeg',
                  width: 300,
                  height: 300,
                ),
              ),
              curve: Curves.ease,
            ),
            AnimatedOpacity(
              duration: Duration(milliseconds: 5000),
              opacity: opacity2,
              child: ClipOval(
                child: Image.asset(
                  'images/beauty2.jpeg',
                  width: 300,
                  height: 300,
                ),
              ),
              curve: Curves.ease,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Text(
          '变',
          style: TextStyle(
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
        onPressed: () {
          setState(() {
            opacity1 = 0.0;
            opacity2 = 1.0;
          });
        },
      ),
    );
  }
}

class BubblesDemo extends StatefulWidget {
  const BubblesDemo({Key? key}) : super(key: key);

  @override
  _BubblesDemoState createState() => _BubblesDemoState();
}

class _BubblesDemoState extends State<BubblesDemo> {
  var opacity = 1.0;
  var size = 10.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('气泡效果动画'),
      ),
      body: Center(
        child: AnimatedOpacity(
          duration: Duration(milliseconds: 100),
          opacity: opacity,
          child: ClipOval(
            child: Bubble(
              size: size,
            ),
          ),
          curve: Curves.ease,
          onEnd: () {
            if (size < 300) {
              setState(() {
                opacity = opacity - 0.002;
                size = size < 300.0 ? size + 1 : size;
                print(size);
              });
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Text(
          'Show',
          style: TextStyle(
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
        onPressed: () {
          setState(() {
            opacity = 0.99;
          });
        },
      ),
    );
  }
}

class Bubble extends StatelessWidget {
  final locationY;
  final size;
  const Bubble({Key? key, this.size = 100.0, this.locationY = 0.0})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: Container(
        width: size * 1.5,
        height: size,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            // radius: 0.5,
            // focalRadius: 0.2,
            begin: Alignment.topLeft,
            end: Alignment.bottomCenter,
            colors: [
              Colors.blue[50]!,
              Colors.green[100]!,
              Colors.yellow[100]!,
              Colors.orange[200]!,
              Colors.red[100]!,
            ],
          ),
        ),
      ),
    );
  }
}
