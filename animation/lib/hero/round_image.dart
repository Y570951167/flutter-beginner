import 'package:flutter/material.dart';

class RoundImage extends StatelessWidget {
  final VoidCallback onTap;
  final assetImageName;
  final imageSize;
  const RoundImage(
      {Key? key,
      required this.onTap,
      required this.assetImageName,
      required this.imageSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: ClipOval(
        child: Image.asset(
          assetImageName,
          width: imageSize,
          height: imageSize,
        ),
      ),
      onTap: onTap,
    );
  }
}

class RoundNetworkImage extends StatelessWidget {
  final VoidCallback onTap;
  final imageUrl;
  final imageSize;
  const RoundNetworkImage(
      {Key? key,
      required this.onTap,
      required this.imageUrl,
      required this.imageSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: ClipOval(
        child: Image.network(
          imageUrl,
          width: imageSize,
          height: imageSize,
        ),
      ),
      onTap: onTap,
    );
  }
}
