import 'package:flutter/material.dart';

class StaggeredAnimationDemo extends StatefulWidget {
  StaggeredAnimationDemo({Key? key}) : super(key: key);

  @override
  _StaggeredAnimationDemoState createState() => _StaggeredAnimationDemoState();
}

class _StaggeredAnimationDemoState extends State<StaggeredAnimationDemo>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _opacity;
  late Animation<double> _width;
  late Animation<double> _height;
  late Animation<Color?> _color;

  @override
  void initState() {
    _controller =
        AnimationController(duration: Duration(seconds: 2), vsync: this)
          ..addListener(() {
            setState(() {});
          });
    _opacity = Tween<double>(begin: 0.5, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(
          0.0,
          0.25,
          curve: Curves.easeIn,
        ),
      ),
    );
    _width = Tween<double>(begin: 0.0, end: 2.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(
          0.25,
          0.5,
          curve: Curves.easeIn,
        ),
      ),
    );
    _height = Tween<double>(begin: 0.0, end: 2.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(
          0.5,
          0.75,
          curve: Curves.easeIn,
        ),
      ),
    );
    _color = ColorTween(begin: Colors.green, end: Colors.blue).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(
          0.75,
          1.0,
          curve: Curves.easeIn,
        ),
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('交错动画'),
      ),
      body: Center(
        child: Opacity(
          opacity: _opacity.value,
          child: Container(
            width: 100 + 100 * _width.value,
            height: 100 + 100 * _height.value,
            color: _color.value,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_arrow),
        onPressed: () {
          if (_controller.isCompleted) {
            _controller.reverse();
          } else if (!_controller.isAnimating) {
            _controller.forward();
          }
        },
      ),
    );
  }
}
