import 'package:flutter/material.dart';

class SlideTransitionDemo extends StatefulWidget {
  SlideTransitionDemo({Key? key}) : super(key: key);

  @override
  _SlideTransitionDemoState createState() => _SlideTransitionDemoState();
}

class _SlideTransitionDemoState extends State<SlideTransitionDemo>
    with SingleTickerProviderStateMixin {
  bool _forward = true;
  final begin = Offset.zero;
  // 第一张图片结束位置移出右侧屏幕
  final end1 = Offset(1.1, 0.0);
  // 第二张图片的初始位置在左侧屏幕
  final begin2 = Offset(-1.1, 0.0);
  late Tween<Offset> tween1 = Tween(begin: begin, end: end1);
  late Tween<Offset> tween2 = Tween(begin: begin2, end: begin);

  late AnimationController _controller =
      AnimationController(duration: const Duration(seconds: 1), vsync: this);

  //使用自定义曲线动画过渡效果
  late Animation<Offset> _animation1 = tween1.animate(
    CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInOut,
    ),
  );
  late Animation<Offset> _animation2 = tween2.animate(CurvedAnimation(
    parent: _controller,
    curve: Curves.easeInOut,
  ));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SlideTransition'),
        brightness: Brightness.dark,
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Stack(
            children: [
              SlideTransition(
                child: ClipOval(
                  child: Image.asset('images/beauty.jpeg'),
                ),
                position: _animation1,
              ),
              SlideTransition(
                child: ClipOval(
                  child: Image.asset('images/beauty2.jpeg'),
                ),
                position: _animation2,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.swap_horizontal_circle_sharp),
        onPressed: () {
          setState(() {
            if (_forward) {
              _controller.forward();
            } else {
              _controller.reverse();
            }
            _forward = !_forward;
          });
        },
      ),
    );
  }
}
