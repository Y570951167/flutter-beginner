import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home_framework/dynamic_detail.dart';
import '../app.dart';
import '../login.dart';
import '../not_found.dart';
import '../splash.dart';

class RouterTable {
  static String splashPath = 'splash';
  static String loginPath = 'login';
  static String homePath = '/';
  static String notFoundPath = '404';
  static String dynamicDetail = 'dynamicDetail';

  static Map<String, WidgetBuilder> routeTables = {
    //404页面
    notFoundPath: (context) => NotFound(),
    //启动页
    splashPath: (context) => Splash(),
    //登录
    loginPath: (context) => LoginPage(),
    //首页
    homePath: (context) => AppHomePage(),
    //动态详情
    dynamicDetail: (context) => DynamicDetail(),
  };

  ///路由拦截
  static Route onGenerateRoute<T extends Object>(RouteSettings settings) {
    var arguments = settings.arguments as Map<String, dynamic>;
    if (arguments != null) {
      arguments['event'] = '路由拦截增加的参数';
    }
    RouteSettings newSettings =
        settings.copyWith(name: settings.name, arguments: arguments);

    return CupertinoPageRoute<T>(
      settings: newSettings,
      builder: (context) {
        String name = settings.name;
        if (routeTables[name] == null) {
          name = notFoundPath;
        }

        Widget widget = routeTables[name](context);

        return widget;
      },
    );
  }
}
