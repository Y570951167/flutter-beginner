import 'package:flutter/material.dart';
import 'package:home_framework/models/dynamic_entity.dart';

class DynamicDetail extends StatelessWidget {
  final DynamicEntity dynamicEntity;
  const DynamicDetail(this.dynamicEntity, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('动态详情'),
        brightness: Brightness.dark,
      ),
      body: Center(
        child: Text("产品 id: ${dynamicEntity.id}"),
      ),
    );
    // onWillPop: () async {
    //   Navigator.of(context).pop({'id': dynamicEntity.id});
    //   return true;
    // },
  }
}
