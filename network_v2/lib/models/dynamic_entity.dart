import 'package:home_framework/api/upload_service.dart';

class DynamicEntity {
  static const IMAGE_BASE_URL = UploadService.uploadBaseUrl + 'image' + '/';
  String _title;
  String _imageUrl;
  int _viewCount;
  String _id;
  String _content;

  get title => _title;
  set title(value) => _title = value;

  get imageUrl => _imageUrl;
  set imageUrl(value) => _imageUrl = IMAGE_BASE_URL + value;

  get viewCount => _viewCount;
  set viewCount(value) => _viewCount = value;

  get content => _content;
  set content(value) => _content = value;

  get id => _id;

  static DynamicEntity fromJson(Map<String, dynamic> json) {
    DynamicEntity newEntity = DynamicEntity();
    newEntity._id = json['_id'];
    newEntity._title = json['title'];
    newEntity._imageUrl = json['imageUrl'];
    // 兼容旧的后端mock产生的数据，后续统一返回图片文件 id。
    if (newEntity._imageUrl != null &&
        !newEntity._imageUrl.startsWith('http')) {
      newEntity._imageUrl = IMAGE_BASE_URL + newEntity._imageUrl;
    }
    newEntity._viewCount = json['viewCount'] ?? 0;
    newEntity._content = json['content'];

    return newEntity;
  }
}
