import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:home_framework/routers/fluro_router.dart';
import 'components/button_util.dart';

import 'api/auth_service.dart';

class CategoryPage extends StatelessWidget {
  void _checkSession() async {
    var response = await AuthService.checkSession();
    if (response != null && response.statusCode == 200) {
      print(response.data);
      EasyLoading.showSuccess('验票通过，持票人：' + response.data['loginUser']);
    } else {
      print('Request Failed');
    }
  }

  void _logout() async {
    var response = await AuthService.logout();
    if (response != null && response.statusCode == 200) {
      EasyLoading.showSuccess('已退出登录');
    } else {
      print('logout Failed');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('验火车票啦', style: Theme.of(context).textTheme.headline4),
        brightness: Brightness.dark,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ButtonUtil.primaryTextButton(
              '买票去',
              () {
                RouterManager.router.navigateTo(
                    context, RouterManager.loginPath,
                    transition: TransitionType.inFromTop);
              },
              context,
              height: 50,
              width: 150,
            ),
            SizedBox(
              height: 5,
            ),
            ButtonUtil.primaryTextButton(
              '验票',
              () {
                // ToDo
                _checkSession();
              },
              context,
              height: 50,
              width: 150,
            ),
            SizedBox(
              height: 5,
            ),
            ButtonUtil.primaryTextButton(
              '下车',
              () {
                _logout();
              },
              context,
              height: 50,
              width: 150,
            ),
            SizedBox(
              height: 5,
            ),
            ButtonUtil.primaryTextButton(
              '文件下载',
              () {
                RouterManager.router
                    .navigateTo(context, RouterManager.downloadPath);
              },
              context,
              height: 50,
              width: 150,
            ),
          ],
        ),
      ),
    );
  }
}
