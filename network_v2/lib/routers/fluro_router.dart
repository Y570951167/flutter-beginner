import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import 'package:home_framework/app.dart';
import 'package:home_framework/appointment.dart';
import 'package:home_framework/dynamic_detail.dart';
import 'package:home_framework/dynamic_edit.dart';

import 'package:home_framework/login.dart';
import 'package:home_framework/not_found.dart';
import 'package:home_framework/routers/permission_denied.dart';
import 'package:home_framework/routers/permission_router.dart';
import 'package:home_framework/splash.dart';
import 'package:home_framework/transition.dart';
import 'package:home_framework/file_download.dart';

import '../dynamic_add.dart';

class RouterManager {
  static String splashPath = '/';
  static String loginPath = '/login';
  static String homePath = '/home';
  static String dynamicPath = '/dynamic';
  static String dynamicEditPath = '$dynamicPath/edit/:id';
  static String dynamicAddPath = '$dynamicPath/add';
  static String dynamicDetailPath = '$dynamicPath/:id';
  static String transitionPath = '/transition';
  static String datingPath = '/dating';
  static String permissionDeniedPath = '/403';
  static String downloadPath = '/download';

  static PermissionRouter router;

  static void initRouter({List<String> whiteList}) {
    if (router == null) {
      router = PermissionRouter();
      defineRoutes(whiteList: whiteList);
    }
  }

  static final routeTable = {
    loginPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return LoginPage();
    }),
    dynamicEditPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return DynamicEdit(params['id'][0]);
    }),
    dynamicAddPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return DynamicAdd();
    }),
    dynamicDetailPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return DynamicDetailPage(params['id'][0]);
    }),
    splashPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return Splash();
    }),
    transitionPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return TransitionPage();
    }),
    datingPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return AppointmentPage();
    }),
    downloadPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return FileDownlaodPage();
    }),
    homePath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return AppHomePage();
    }),
    permissionDeniedPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return PermissionDenied();
    }),
  };

  static void defineRoutes({List<String> whiteList}) {
    router.whiteList = whiteList;
    router.permissionDeniedPath = permissionDeniedPath;
    routeTable.forEach((path, handler) {
      router.define(path, handler: handler);
    });

    router.notFoundHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return NotFound();
    });
  }
}
