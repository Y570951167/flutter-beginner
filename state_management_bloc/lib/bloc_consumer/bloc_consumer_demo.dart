import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/login_cubit.dart';

class BlocConsumerWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => LoginCubit(),
      child: BlocConsumerDemo(),
    );
  }
}

class BlocConsumerDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BlocConsumer示例'),
      ),
      body: Center(
        child: BlocConsumer<LoginCubit, LoginStatus>(
          listener: (context, loginSatus) async {
            if (loginSatus == LoginStatus.logout ||
                loginSatus == LoginStatus.logon) {
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(SnackBar(
                  content:
                      Text(loginSatus == LoginStatus.logout ? '已退出登录' : '登录成功'),
                  duration: Duration(seconds: 1),
                ));
            } else {
              var confirmed = await _confirmLogout(context);
              if (confirmed == true) {
                context.read<LoginCubit>().logout();
              }
            }
          },
          builder: (context, loginSatus) => TextButton(
            child: Text(
              loginSatus == LoginStatus.logon ? '退出登录' : '登录',
              style: TextStyle(
                fontSize: 24.0,
              ),
            ),
            onPressed: () {
              if (loginSatus == LoginStatus.logon) {
                context.read<LoginCubit>().logoutConfirm();
              } else {
                context.read<LoginCubit>().login();
              }
            },
          ),
        ),
      ),
    );
  }

  Future<bool?> _confirmLogout(BuildContext context) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(' 确认退出吗'),
          content: Text('呜呜呜！退出了就没人陪我玩了！'),
          actions: <Widget>[
            TextButton(
              child: Text(
                '直接退出',
                style: TextStyle(color: Colors.grey),
              ),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
            TextButton(
              child: Text('我再想想'),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
          ],
        );
      },
    );
  }
}
