import 'package:flutter/material.dart';
import 'package:home_framework/components/button_util.dart';

class SideBar extends StatelessWidget {
  const SideBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Colors.transparent,
        ),
        Positioned(
          left: 0,
          width: MediaQuery.of(context).size.width / 2,
          top: 0,
          bottom: 0,
          child: Container(
            color: Colors.white,
            child: Center(
              child: ButtonUtil.primaryTextButton('返回', () {
                Navigator.of(context).pop();
              }, context),
            ),
          ),
        ),
      ],
    );
  }
}
