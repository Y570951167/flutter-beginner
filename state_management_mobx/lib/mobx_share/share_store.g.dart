// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'share_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ShareStore on ShareStoreBase, Store {
  final _$praiseCountAtom = Atom(name: 'ShareStoreBase.praiseCount');

  @override
  int get praiseCount {
    _$praiseCountAtom.reportRead();
    return super.praiseCount;
  }

  @override
  set praiseCount(int value) {
    _$praiseCountAtom.reportWrite(value, super.praiseCount, () {
      super.praiseCount = value;
    });
  }

  final _$favorCountAtom = Atom(name: 'ShareStoreBase.favorCount');

  @override
  int get favorCount {
    _$favorCountAtom.reportRead();
    return super.favorCount;
  }

  @override
  set favorCount(int value) {
    _$favorCountAtom.reportWrite(value, super.favorCount, () {
      super.favorCount = value;
    });
  }

  final _$ShareStoreBaseActionController =
      ActionController(name: 'ShareStoreBase');

  @override
  void increamentPraise() {
    final _$actionInfo = _$ShareStoreBaseActionController.startAction(
        name: 'ShareStoreBase.increamentPraise');
    try {
      return super.increamentPraise();
    } finally {
      _$ShareStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void increamentFavor() {
    final _$actionInfo = _$ShareStoreBaseActionController.startAction(
        name: 'ShareStoreBase.increamentFavor');
    try {
      return super.increamentFavor();
    } finally {
      _$ShareStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
praiseCount: ${praiseCount},
favorCount: ${favorCount}
    ''';
  }
}
