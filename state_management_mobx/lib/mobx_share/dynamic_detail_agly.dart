import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'share_store.dart';

class DynamicDetailWrapper extends StatelessWidget {
  final store = ShareStore();
  DynamicDetailWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('build');
    return Scaffold(
      appBar: AppBar(
        title: Text('局部 Store'),
      ),
      body: Stack(
        children: [
          Container(height: 300, color: Colors.red),
          Positioned(
              bottom: 0,
              height: 60,
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  _PraiseButton(store: store),
                  _FavorButton(store: store),
                ],
              ))
        ],
      ),
    );
  }
}

class _FavorButton extends StatelessWidget {
  final ShareStore store;
  const _FavorButton({Key? key, required this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('FavorButton');
    return Container(
      alignment: Alignment.center,
      color: Colors.blue,
      child: TextButton(
        onPressed: () {
          store.increamentFavor();
        },
        child: Observer(
          builder: (context) => Text(
            '收藏 ${store.favorCount}',
            style: TextStyle(color: Colors.white),
          ),
        ),
        style: ButtonStyle(
            minimumSize: MaterialStateProperty.resolveWith(
                (states) => Size((MediaQuery.of(context).size.width / 2), 60))),
      ),
    );
  }
}

class _PraiseButton extends StatelessWidget {
  final ShareStore store;
  const _PraiseButton({Key? key, required this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('PraiseButton');
    return Container(
      alignment: Alignment.center,
      color: Colors.green[400],
      child: TextButton(
        onPressed: () {
          store.increamentPraise();
        },
        child: Observer(
          builder: (context) => Text(
            '点赞 ${store.praiseCount}',
            style: TextStyle(color: Colors.white),
          ),
        ),
        style: ButtonStyle(
            minimumSize: MaterialStateProperty.resolveWith(
                (states) => Size((MediaQuery.of(context).size.width / 2), 60))),
      ),
    );
  }
}
