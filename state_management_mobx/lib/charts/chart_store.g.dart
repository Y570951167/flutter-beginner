// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chart_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ChartStore on ChartStoreBase, Store {
  final _$lineYDataAtom = Atom(name: 'ChartStoreBase.lineYData');

  @override
  List<double> get lineYData {
    _$lineYDataAtom.reportRead();
    return super.lineYData;
  }

  @override
  set lineYData(List<double> value) {
    _$lineYDataAtom.reportWrite(value, super.lineYData, () {
      super.lineYData = value;
    });
  }

  final _$featchLineDataAsyncAction =
      AsyncAction('ChartStoreBase.featchLineData');

  @override
  Future<void> featchLineData() {
    return _$featchLineDataAsyncAction.run(() => super.featchLineData());
  }

  @override
  String toString() {
    return '''
lineYData: ${lineYData}
    ''';
  }
}
