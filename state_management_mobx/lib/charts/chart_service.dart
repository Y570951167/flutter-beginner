import '../utils/http_util.dart';

class ChartService {
  static Future getLines() async {
    var result = await HttpUtil.get(
      'mock/trend',
    );

    return result;
  }
}
